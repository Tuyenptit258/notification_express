'use strict'

const util = require('util')
const mysql = require('mysql')
const db = require('../api/db')

const table = 'notification'

module.exports = {
    get: (req, res) => {
        let sql = 'SELECT * FROM notification'
        db.query(sql, (err, response) => {
            if (err) throw err
            res.json(response)
        })
    },
    detail: (req, res) => {
        let sql = 'SELECT * FROM notification WHERE id = ?'
        db.query(sql, [req.params.notificationId], (err, response) => {
            if (err) throw err
            res.json(response[0])
        })
    },
    update: (req, res) => {
        let data = req.body;
        let notificationId = req.params.notificationId;
        let sql = 'UPDATE notification SET ? WHERE id = ?'
        db.query(sql, [data, notificationId], (err, response) => {
            if (err) throw err
            res.json({message: 'Update success!'})
        })
    },
    store: (req, res) => {
        let data = req.body;
        let sql = 'INSERT INTO notification SET ?'
        db.query(sql, [data], (err, response) => {
            if (err) throw err
            res.json({message: 'Insert success!'})
        })
    },
    delete: (req, res) => {
        let sql = 'DELETE FROM notification WHERE id = ?'
        db.query(sql, [req.params.notificationId], (err, response) => {
            if (err) throw err
            res.json({message: 'Delete success!'})
        })
    }
}