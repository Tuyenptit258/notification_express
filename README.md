# Api nodejs example
# Install package
Run `npm install`

# Run api
Run `npm run start`

# tạo database nodejs_api với bảng là notification
các cột là id, username, user_id, post_id, content_post, user_avatar
CREATE DATABASE nodejs_api;

CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `post_id` varchar(255) DEFAULT NULL,
  `content_post` varchar(255) DEFAULT NULL,
  `user_avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) 