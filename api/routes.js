
module.exports = function(app) {
  var productsCtrl = require('../controllers/ProductsController');

  // todoList Routes
  app.route('/notification')
    .get(productsCtrl.get)
    .post(productsCtrl.store);


  app.route('/notification/:notificationId')
    .get(productsCtrl.detail)
    .put(productsCtrl.update)
    .delete(productsCtrl.delete);
};
